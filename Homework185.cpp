
#include <iostream>

template <class T>
class Stack
{
public:
    Stack()
    {
        m_Array = nullptr;
    }

    ~Stack()
    {
        delete[] m_Array;
    }

    void pop() 
    {
        if ((m_Array == nullptr) || (m_Size == 0))
        {
            std::cout << "Nothing to remove\n";
			return;
        }

        rmData_();
    }

    void push(T val) 
    {
        if (m_Size == 0) {
            ++m_Size;
            m_Array = new T[m_Size];
            m_Array[0] = val;
        }
        else {
            addData_(val);
        }

    }

    void print()
    {
        if (!m_Array)
            return;

        if (m_Size == 0)
        {
			std::cout << "Stack is empty\n";
            return;
        }

        std::cout << "Stack data:\n";
        for (T* p = m_Array; p < (m_Array + m_Size); p++) {
            std::cout << *p << ' ';
        }
        std::cout << '\n';
    }

    int getLength()
    {
        return m_Size;
    }

private:

    void rmData_()
    {
        --m_Size;
        T* newArray = new T[m_Size];

		// copy prev data
		for (int i = 0; i < m_Size; ++i) {
			newArray[i] = m_Array[i];
		}

		delete[] m_Array;
		m_Array = newArray;
    }

    void addData_(T val)
    {
        ++m_Size;
        T* newArray = new T[m_Size];

        // copy prev data
        for (int i = 0; i < m_Size-1; ++i) {
            newArray[i] = m_Array[i];
		}
        newArray[m_Size - 1] = val;

        delete[] m_Array;
        m_Array = newArray;
    }

    T* m_Array;
    int m_Size{ 0 };

};

int main()
{
    
    Stack<char> stack; 
    
    stack.push(4);
    stack.push(5);
	stack.push(10);
	stack.push(34);
	stack.push(65);

    stack.print();

    stack.pop();
	stack.pop();
	stack.pop();

    stack.print();

	stack.push(423);
    stack.pop();
    stack.push(34);
	stack.push(511);

    stack.print();

    stack.pop();
    stack.pop();
    stack.pop();
    stack.pop();

    stack.print();

	stack.pop();
	stack.pop();


}
